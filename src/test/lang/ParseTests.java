package lang;

import java.io.File;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

// public class ParseTests {
// 	/** Directory where the test input files are stored. */
// 	private static final File TEST_DIRECTORY = new File("testfiles/parser");

// 	@Test public void example() {
// 		Util.testValidSyntax(TEST_DIRECTORY, "example.in");
// 	}

// 	@Test
// 	public void error() {
// 		Util.testSyntaxError(TEST_DIRECTORY, "error.in");
// 	}
// }

@RunWith(Parameterized.class)
public class ParseTests {
	/** Directory where the test input files are stored. */
	private static final File TEST_DIRECTORY = new File("testfiles/parser");

	private final String filename;
	public ParseTests(String testFile) {
		filename = testFile;
	}

	@Test public void runTest() throws Exception {
		Util.parse(new File(TEST_DIRECTORY, filename));
		// String actual = program.dumpTree();
		// Util.compareOutput(actual,
		// 		new File(TEST_DIRECTORY, Util.changeExtension(filename, ".out")),
		// 		new File(TEST_DIRECTORY, Util.changeExtension(filename, ".expected")));
	}

	@Parameters(name = "{0}")
	public static Iterable<Object[]> getTests() {
		return Util.getTestParameters(TEST_DIRECTORY, ".in");
	}
}