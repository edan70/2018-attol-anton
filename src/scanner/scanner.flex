package lang.ast; // The generated scanner will belong to the package lang.ast

import lang.ast.LangParser.Terminals; // The terminals are implicitly defined in the parser
import lang.ast.LangParser.SyntaxError;

%%

// define the signature for the generated scanner
%public
%final
%class LangScanner
%extends beaver.Scanner

// the interface between the scanner and the parser is the nextToken() method
%type beaver.Symbol
%function nextToken

// store line and column information in the tokens
%line
%column

// this code will be inlined in the body of the generated scanner class
%{
  private beaver.Symbol sym(short id) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }
%}

// macros
Comment = \/\/[^\n]*|\/\*[^\*\/]*\*\/
WhiteSpace = [ ] | \t | \f | \n | \r
Numeral = 0|[1-9][0-9]*
ID = [a-z][a-zA-Z0-9_]*
CLASS_ID = [A-Z][a-zA-Z0-9_]*
STRINGLIT =\"[^\"]*\"
%%

// discard comment information
{Comment}  { }
// discard whitespace information
{WhiteSpace}  { }

// token definitions
"{"           { return sym(Terminals.LBRACKET); }
"}"           { return sym(Terminals.RBRACKET); }
"("           { return sym(Terminals.LPAREN); }
")"           { return sym(Terminals.RPAREN); }
","           { return sym(Terminals.COMMA); }
";"           { return sym(Terminals.SEMIC); }
"="           { return sym(Terminals.ASSIGN); }
":="          { return sym(Terminals.ASSIGNC); }
"+"           { return sym(Terminals.PLUS); }
"-"           { return sym(Terminals.MINUS); }
"*"           { return sym(Terminals.MUL); }
"/"           { return sym(Terminals.DIV); }
"=="          { return sym(Terminals.EQ); }
"!="          { return sym(Terminals.NEQ); }
">="          { return sym(Terminals.GTEQ); }
">"           { return sym(Terminals.GT); }
"<="          { return sym(Terminals.LTEQ); }
"<"           { return sym(Terminals.LT); }
"."           { return sym(Terminals.DOT); }
"NULL"        { return sym(Terminals.NULL); }
"not"         { return sym(Terminals.NOT); }
"class"       { return sym(Terminals.CLASS); }
"while"       { return sym(Terminals.WHILE); }
"if"          { return sym(Terminals.IF); }
"else"        { return sym(Terminals.ELSE); }
"continue"    { return sym(Terminals.CONTINUE); }
"break"       { return sym(Terminals.BREAK); }
"return"      { return sym(Terminals.RETURN); }
"int"         { return sym(Terminals.INT); }
"extends"     { return sym(Terminals.EXTENDS); }
"obj"         { return sym(Terminals.OBJ); }
"super"       { return sym(Terminals.SUPER); }
{CLASS_ID}    { return sym(Terminals.CLASS_ID); }
{Numeral}     { return sym(Terminals.NUMERAL); }
{STRINGLIT}   { return sym(Terminals.STRINGLIT); }
{ID}          { return sym(Terminals.ID); }
<<EOF>>       { return sym(Terminals.EOF); }

/* error fallback */
[^]           { throw new SyntaxError("Illegal character <"+yytext()+">"); }
