aspect TypeAnalysis {
  syn Type Expr.type();
  inh Type IdDecl.type();

  eq VarDecl.getIdDecl().type() = getType().type();
  eq FuncDecl.getIdDecl().type() = getType().type();
  eq ClassDecl.getIdDecl().type() = objType();
  eq Formal.getIdDecl().type() = getType().type();
  eq ArithmeticBinExpr.type() {
    Type l =  getLeft().type();
    Type r =  getRight().type();
    if(r.isIntType() && l.isIntType())
      return intType() ;
    else if(!r.isUnknownType() && l.isObjType() || !l.isUnknownType() && r.isObjType())
      return objType();
    return unknownType();
  }

  eq FuncUse.type() {
    FuncDecl f = getIdUse().decl().function();
    for(Stmt s: f.getBlock().getStmts()){
        if(s instanceof Return) {
          Return r = (Return) s;
          if(r.hasExpr())
            return r.getExpr().type();
        }
    }
    return getIdUse().type();
  }

  eq BinExpr.type() {
      return boolType();
  }
  eq BinExpr.getChild().type() {
      return boolType();
  }

  eq Numeral.type() = intType();
  eq StringLit.type() = objType();
  eq Not.type() = getExpr().type();
  eq IdUseExpr.type() = decl().type();
  eq DotExpr.type() = intType();
  eq DotExpr.getChild().expectedType() = objType();
  eq FieldAccess.type() = objType();
  eq MethodAccess.type() = getFuncUse().type();
  eq NewInstance.type() = getIdUse().type();
  eq NullExpr.type() = nullType();

  // eq Assignment.getIdAccess().type() = getIdAccess().type();
  eq Assignment.getExpr().expectedType() = getIdAccess().type();
  syn Type IdAccess.type();
  eq IdUse.type() = decl().type();
  eq Dot.type() = objType();
  eq FieldIdAccess.type() = objType();
  eq MethodIdAccess.type() = getFuncUse().type();

  /* Expected types for Statements */
  eq Program.getChild().expectedType() = unknownType();
  eq ArithmeticBinExpr.getChild().expectedType() = objType();
  eq BinExpr.getChild().expectedType() = objType();

  eq Stmt.getChild().expectedType() = objType();
  eq VarDecl.getChild().type() = getType().type();
  eq VarDecl.getChild().expectedType() = getType().type();
  eq WhileStmt.getCondition().expectedType() = boolType();
  eq IfStmt.getCondition().expectedType() = boolType();
  inh Type Expr.expectedType();

  // /* Is Type Error for Statements */
  syn boolean Type.compatibleType(Type type) {
    if (this == type) {
      return true;
    }
    if (this instanceof IntType && type instanceof ObjType || this instanceof NullType && type instanceof ObjType)
      return true;
    return false;
  } 
    
  syn boolean Expr.isTypeError() = !type().compatibleType(expectedType());

  // /* Is Variable or Function */
  inh boolean IdDecl.isVariable();
  inh boolean IdDecl.isFunction();
  inh boolean IdDecl.isClass();

  
  syn boolean FuncUse.isFunction() {
    IdDecl decl = getIdUse().decl();
    return decl.isUnknown() ? false : decl.isFunction();
  }

  // syn boolean IdUse.isUnknownFunctionError() = functionExpected() == isFunction();

  syn boolean FuncUse.correctNumFormals() {
    if (!isFunction()) return true;
    return getNumExpr() == getIdUse().decl().function().getNumFormal();
  }

  syn boolean IdUse.isFunction() = decl().isFunction();
  syn boolean IdUse.isVariable() = decl().isVariable();
  syn boolean IdUse.isClass() = decl().isClass();
 
  syn FuncDecl IdUse.function() = decl().function();
  inh FuncDecl IdDecl.function();
	eq FuncDecl.getIdDecl().function() = this;
	eq Program.getChild().function() = unknownFuncDecl();
  
  eq VarDecl.getIdDecl().isVariable() = true;
  eq VarDecl.getIdDecl().isFunction() = false;
  eq VarDecl.getIdDecl().isClass() = false;

  eq FuncDecl.getIdDecl().isFunction() = true;
  eq FuncDecl.getIdDecl().isVariable() = false;
  eq FuncDecl.getIdDecl().isClass() = false;

  eq Formal.getIdDecl().isVariable() = true;
  eq Formal.getIdDecl().isFunction() = false;
  eq Formal.getIdDecl().isClass() = false;

  eq ClassDecl.getIdDecl().isVariable() = false;
  eq ClassDecl.getIdDecl().isFunction() = false;
  eq ClassDecl.getIdDecl().isClass() = true;

  eq Program.getChild().isVariable() = false;
  eq Program.getChild().isFunction() = false;
  eq Program.getChild().isClass() = false;

  eq Program.getChild().enclosingFunction() = unknownFuncDecl();
  inh FuncDecl Block.enclosingFunction();
  eq FuncDecl.getBlock().enclosingFunction() = this;

  inh FuncDecl Stmt.enclosingFunction();
  inh FuncDecl Expr.enclosingFunction();

  syn Type FuncDecl.type() = getType().type();

}
