package lang;

import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;

import beaver.Parser.Exception;

import lang.ast.Program;
import lang.ast.LangParser;
import lang.ast.LangScanner;
import lang.ast.ErrorMessage;

/**
 * Dumps the parsed Abstract Syntax Tree of a Calc program.
 */
public class Interpreter {
	/**
	 * Entry point
	 * 
	 * @param args
	 */

	public static Object DrAST_root_node; // Enable debugging with DrAST

	public static void main(String[] args) {
		try {
			if (args.length != 1) {
				System.err.println("You must specify a source file on the command line!");
				printUsage();
				System.exit(1);
				return;
			}
			long startTime = System.currentTimeMillis();

			String filename = args[0];
			LangScanner scanner = new LangScanner(new FileReader(filename));
			LangParser parser = new LangParser();
			Program program = (Program) parser.parse(scanner);
			DrAST_root_node = program; // Enable debugging with DrAST
			if (program.errors().size() == 0) {
				// System.out.println(program.dumpTree());
				program.eval();
			}
			for (ErrorMessage m : program.errors()) {
				System.out.println(m);
			}
			int i = 0;
			// while (i < 100) {
			// 	i++;
			// 	startTime = System.currentTimeMillis();
			// 	program.eval();
			// 	long stopTime = System.currentTimeMillis();
			// 	long elapsedTime = stopTime - startTime;
			// 	System.out.println(elapsedTime);
			// }
			// }
		} catch (

		FileNotFoundException e) {
			System.out.println("File not found!");
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace(System.err);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void printUsage() {
		System.err.println("Usage: DumpTree FILE");
		System.err.println("  where FILE is the file to be parsed");
	}
}
