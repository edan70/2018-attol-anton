## Building

This project is built with gradle. To build simply run

```
./gradlew build
```

To generate the jar file, run:

```
./gradlew jar
```

## Test

To run the automatic test suite, simply run

```
./gradlew test
```

## Running the program

To build and run the program you can use the run script. Example:

```
./run.sh testfiles/examples/superclass.in
```

Or in two steps:

```
./gradlew jar
java -jar interpreter.jar testfiles/examples/superclass.in
```

## Examples

Example programs can be found in testfiles/examples

## License

This repository is covered by the license BSD 2-clause, see file LICENSE
