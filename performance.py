import subprocess
import sys
import scipy.stats as st
import numpy as np
import matplotlib.pyplot as plt

command = 'java -jar interpreter.jar testfiles/interpreter/bigwhile.in'
if len(sys.argv) > 1:
    command = sys.argv[1]

n = 1
data = []
status_checks = 5
result = subprocess.run(command.split(
    ' '), stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout.decode('UTF-8').strip()
data.extend([float(x) / 1000 for x in result.split('\n')])
# if i % (n // status_checks) == 0:
#     print("{}% done".format((i / n) * 100))
ci = st.t.interval(0.95, len(data)-1, loc=np.mean(data), scale=st.sem(data))
print(ci)

plt.figure()
plt.hist(data)
one_x12, one_y12 = [ci[0], ci[0]], [0, 20]
# cnfidence interval right line
two_x12, two_y12 = [ci[1], ci[1]], [0, 20]

plt.plot(one_x12, one_y12, two_x12, two_y12, marker='o')
plt.show()
